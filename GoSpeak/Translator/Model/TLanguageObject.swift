//
//  TLanguageObject.swift
//  Translator
//
//  Created by Beniamin Muntean on 29/05/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit

public class TLanguageObject: NSObject {
    public var languageRoundFlagIconName: String
    public var languageRectangleFlagIconName: String
    public var languageIdentifier: String
    public var languageName: String
    public var languageCode: String
    
    public init(languageRoundFlagName: String, languageRectangleFlagName: String, languageIdentifier: String, languageName: String, languageCode: String) {
        self.languageIdentifier = languageIdentifier
        self.languageRoundFlagIconName = languageRoundFlagName
        self.languageRectangleFlagIconName = languageRectangleFlagName
        self.languageName = languageName
        self.languageCode = languageCode
    }

}
