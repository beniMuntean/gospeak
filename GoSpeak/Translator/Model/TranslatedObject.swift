//
//  TranslatedObject.swift
//  Translator
//
//  Created by Beniamin Muntean on 18/05/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit

public class TranslatedObject: NSObject {
    public var flagIconName: String
    public var languageName: String
    public var message: String
    
    public init(flagIconName: String, languageName: String, message: String) {
        self.flagIconName = flagIconName
        self.languageName = languageName
        self.message = message
    }
}
