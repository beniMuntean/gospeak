//
//  TAudioDelegate.swift
//  Translator
//
//  Created by Beniamin Muntean on 16/05/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit
import Foundation

public protocol TAudioDelegate {
        // Classes that adopt this protocol MUST define
        // this method -- and hopefully do something in
        // that definition.
    func didFinishRecording(text: String)
    func didRetrievePartialResult(text: String)
}
