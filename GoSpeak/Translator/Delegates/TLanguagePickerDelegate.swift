//
//  TLanguagePickerDelegate.swift
//  Translator
//
//  Created by Beniamin Muntean on 18/05/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit
import Foundation

public protocol TLanguagePickerDelegate {
    // Classes that adopt this protocol MUST define
    // this method -- and hopefully do something in
    // that definition.
    
    func languagePickerDidFinishSelectLanguage(languagePiker: TLanguagePickerView, language: TLanguageObject, languageTag: Int)
}
