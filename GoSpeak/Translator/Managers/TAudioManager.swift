//
//  TAudioManager.swift
//  Translator
//
//  Created by Beniamin Muntean on 16/05/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit
import Speech
import AVKit

public class TAudioManager: NSObject, AVSpeechSynthesizerDelegate, SFSpeechRecognizerDelegate {
    
    public static let sharedInstance = TAudioManager()
    
    var audioPlayer = AVAudioPlayer()
    let speechSynthesizer = AVSpeechSynthesizer()
    var rate: Float!
    var pitch: Float!
    var volume: Float!
    var totalUtterances: Int! = 0
    var currentUtterance: Int! = 0
    var totalTextLength: Int = 0
    var spokenTextLengths: Int = 0
    var arrVoiceLanguages: [Dictionary<String, String?>] = []
    var selectedVoiceLanguage = 0
    var previousSelectedRange: NSRange!
    var recognitionRequest: SFSpeechAudioBufferRecognitionRequest!
    var recognitionTask: SFSpeechRecognitionTask?
    let audioEngine = AVAudioEngine()
    var speakEnabled: Bool = false
    var recordedString: String = ""
    public var delegate: TAudioDelegate?
    
    override init() {
        super.init()
        
        //speechRecognizer.delegate = self
        speechSynthesizer.delegate = self
        registerDefaultSettings()
    }
    
    public func checkSpeechAuthorization() {
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            switch authStatus {
            case .authorized:
                self.speakEnabled = true
            case .denied:
                print("User denied access to speech recognition")
            case .restricted:
                print("Speech recognition restricted on this device")
            case .notDetermined:
                print("Speech recognition not yet authorized")
            }
        }
    }
    
    func registerDefaultSettings() {
        rate = AVSpeechUtteranceDefaultSpeechRate
        pitch = 1
        volume = 1.0
        
        let defaultSpeechSettings: Dictionary<String, AnyObject> = ["rate": rate as AnyObject, "pitch": pitch as AnyObject, "volume": volume as AnyObject]
        
        UserDefaults.standard.register(defaults: defaultSpeechSettings)
    }

    public func speakText(text: String, language: String) {
        if !speechSynthesizer.isSpeaking {
            let textParagraphs = text.components(separatedBy: "\n")
            totalUtterances = textParagraphs.count
            currentUtterance = 0
            totalTextLength = 0
            spokenTextLengths = 0
            
            // to be deleted
//            for voice in AVSpeechSynthesisVoice.speechVoices() {
//                let aStr = String(format: "%@%@ , %@%@, %@%@", "Voice Name: ", voice.name, "Identifier: ", voice.identifier, "Language: ", voice.language)
//                print(aStr)
//
//            }

            for line in textParagraphs {
                let speechUtterance = AVSpeechUtterance(string: line)
                speechUtterance.rate = rate
                speechUtterance.pitchMultiplier = pitch
                speechUtterance.volume = volume
                speechUtterance.postUtteranceDelay = 0.007 // delay after the current text has been spoken
                
                let voice: AVSpeechSynthesisVoice = AVSpeechSynthesisVoice.init(language: language)!
                speechUtterance.voice = voice
                totalTextLength = totalTextLength + line.utf16.count

                speechSynthesizer.speak(speechUtterance)
            }
        }
        else {
            speechSynthesizer.continueSpeaking()
        }
    }
    
    public func pauseSpeak() {
        speechSynthesizer.pauseSpeaking(at: AVSpeechBoundary.immediate)
    }
    
    public func startRecording(language: String) {
    
        let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: language))!
        
        self.speakEnabled = false;
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default)
            try audioSession.setMode(AVAudioSession.Mode.default)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            var isFinal = false
            
            if result != nil {
                let bestString = result!.bestTranscription.formattedString
                self.recordedString = bestString
                self.delegate?.didRetrievePartialResult(text: bestString)
                isFinal = (result!.isFinal)
            }
            
            if error != nil || isFinal {
                self.delegate?.didFinishRecording(text: self.recordedString)
            }
        })
        
        let recordingFormat = self.audioEngine.inputNode.outputFormat(forBus: 0)
        self.audioEngine.inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    
    public func stopRecording() {
        self.audioEngine.inputNode.removeTap(onBus: 0)
        self.audioEngine.stop()
        self.recognitionRequest.endAudio()
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
