//
//  TLanguageManager.swift
//  Translator
//
//  Created by Beniamin Muntean on 30/05/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit
import Speech
//import Firebase

public class TLanguageManager: NSObject {
    
    public static let sharedInstance = TLanguageManager()
    
    public var languagesDataSource = [TLanguageObject]()
    public var leftLanguage: TLanguageObject?
    public var rightLanguage: TLanguageObject?
    public var sourceLanguage: TLanguageObject?
    public var destinationLanguage: TLanguageObject?
    
    
    override init() {
        super.init()
        
//        FirebaseApp.configure()
        prepareDataSource()
    }
    
    func downloadLanguagesDataSource() {
        // download json
    }
    
    public func parseLanguagesJsonFile(jsonFile: String) {
        // parse json
    }
    
    public func prepareDataSource() {
        let supportedLanguages = SFSpeechRecognizer.supportedLocales()
        let arSADict = ["languageName": "Arabic",
                        "languageIdentifier": "ar-SA",
                        "languageCode": "ar"]
        let caESDict = ["languageName": "Catalan",
                        "languageIdentifier": "ca-ES",
                        "languageCode": "ca"]
        let esESDict = ["languageName": "Spanish",
                        "languageIdentifier": "es-ES",
                        "languageCode": "es"]
        let esMXDict = ["languageName": "Spanish (Mexic)",
                        "languageIdentifier": "es-MX",
                        "languageCode": "es"]
        let enAUDict = ["languageName": "English (Australia)",
                        "languageIdentifier": "en-AU",
                        "languageCode": "en"]
        let enCADict = ["languageName": "English (Canada)",
                        "languageIdentifier": "en-CA",
                        "languageCode": "en"]
        let enINDict = ["languageName": "English (India)",
                        "languageIdentifier": "en-IN",
                        "languageCode": "en"]
        let enUKDict = ["languageName": "English (UK)",
                        "languageIdentifier": "en-GB",
                        "languageCode": "en"]
        let enUSDict = ["languageName": "English (US)",
                        "languageIdentifier": "en-US",
                        "languageCode": "en"]
        let frCADict = ["languageName": "French (Canada)",
                        "languageIdentifier": "fr-CA",
                        "languageCode": "fr"]
        let frFRDict = ["languageName": "French",
                        "languageIdentifier": "fr-FR",
                        "languageCode": "fr"]
        let ptBRDict = ["languageName": "Portuguese (Brazilia)",
                        "languageIdentifier": "pt-BR",
                        "languageCode": "pt"]
        let ptPTDict = ["languageName": "Portuguese",
                        "languageIdentifier": "pt-PT",
                        "languageCode": "pt"]
        let zhCNDict = ["languageName": "Chinese (Mandarin)",
                        "languageIdentifier": "zh-CN",
                        "languageCode": "zh-CN"]
        let zhHKDict = ["languageName": "Chinese (Cantonese)",
                        "languageIdentifier": "zh-HK",
                        "languageCode": "zh-CN"]
        let zhTWDict = ["languageName": "Chinese (Taiwan)",
                        "languageIdentifier": "zh-TW",
                        "languageCode": "zh-TW"]
        let afAFDict = ["languageName": "Afrikans",
                        "languageIdentifier": "af-AF",
                        "languageCode": "af"]
        let huHUDict = ["languageName": "Hungarian",
                        "languageIdentifier": "hu-HU",
                        "languageCode": "hu"]
        let czCZDict = ["languageName": "Czech",
                        "languageIdentifier": "cz-CZ",
                        "languageCode": "cs"]
        let daDADict = ["languageName": "Danish",
                        "languageIdentifier": "da-DA",
                        "languageCode": "da"]
        let deATDict = ["languageName": "German (Austria)",
                        "languageIdentifier": "de-AT",
                        "languageCode": "de"]
        let deCHDict = ["languageName": "German (Switzerland)",
                        "languageIdentifier": "de-CH",
                        "languageCode": "de"]
        let deDEDict = ["languageName": "German (Germany)",
                        "languageIdentifier": "de-DE",
                        "languageCode": "de"]
        let elGRDict = ["languageName": "Greek",
                        "languageIdentifier": "el-GR",
                        "languageCode": "el"]
        let heILDict = ["languageName": "Hebrew (Israel)",
                        "languageIdentifier": "he-IL",
                        "languageCode": "iw"]
        let hiINDict = ["languageName": "Hindi (India)",
                        "languageIdentifier": "hi-IN",
                        "languageCode": "hi"]
        let hrhrDict = ["languageName": "Croatian (Croatia)",
                        "languageIdentifier": "hr-HR",
                        "languageCode": "hr"]
        let idIDDict = ["languageName": "Indonesian (Indonesia)",
                        "languageIdentifier": "id-ID",
                        "languageCode": "id"]
        let itITDict = ["languageName": "Italian (Italy)",
                        "languageIdentifier": "it-IT",
                        "languageCode": "it"]
        let jaJPDict = ["languageName": "Japanese (Japan)",
                        "languageIdentifier": "ja-JP",
                        "languageCode": "ja"]
        let koKRDict = ["languageName": "Korean (Korea)",
                        "languageIdentifier": "ko-KR",
                        "languageCode": "ko"]
        let roRODict = ["languageName": "Romanian",
                        "languageIdentifier": "ro-RO",
                        "languageCode": "ro"]
        
        var array = [NSDictionary]()
        array = [arSADict, caESDict, zhCNDict, zhHKDict, zhTWDict, hrhrDict, czCZDict, daDADict, enAUDict, enCADict, enINDict, enUKDict, enUSDict, frCADict, frFRDict, deATDict, deCHDict, deDEDict, elGRDict, heILDict, hiINDict, huHUDict, idIDDict, itITDict, jaJPDict, koKRDict, ptBRDict, ptPTDict, roRODict, esESDict, esMXDict, afAFDict] as [NSDictionary]
        
        for languageDict in array {
            for language in supportedLanguages {
                let languageIdentifier: String = languageDict["languageIdentifier"] as! String

                if languageIdentifier == language.identifier {
                    let flagImageName: String = String(format: "%@-rectangle", languageIdentifier)
                    let languageName = languageDict["languageName"] as! String
                    let languageCode = languageDict["languageCode"] as! String
                    let languageObj: TLanguageObject = TLanguageObject(languageRoundFlagName: languageIdentifier, languageRectangleFlagName: flagImageName, languageIdentifier: languageIdentifier, languageName: languageName, languageCode:languageCode)
                    
                    self.languagesDataSource.append(languageObj)
                }
            }
        }
    }
    
    public func getLanguageObjectFor(languageIdentifier: String!) -> TLanguageObject? {
        var languageObject: TLanguageObject?
        
        if (languageIdentifier != nil) {
            for language in self.languagesDataSource {
                if language.languageIdentifier == languageIdentifier {
                    languageObject = language
                }
            }
        }
        
        return languageObject
    }
    
    public func getIndexFor(language:TLanguageObject) -> Int {
        for i in 0...self.languagesDataSource.count {
            if self.languagesDataSource[i] == language {
                return i
            }
        }
        
        return 0
    }
}
