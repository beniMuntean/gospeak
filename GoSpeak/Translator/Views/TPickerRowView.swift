//
//  TPickerRowView.swift
//  Translator
//
//  Created by Beniamin Muntean on 18/05/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit

public class TPickerRowView: UIView {

    @IBOutlet weak var languageNameLabel: UILabel!
    @IBOutlet weak var flagImageView: UIImageView!
    var view: UIView!
    let nibName = "TPickerRowView"
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetUp()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetUp()
    }
    
    func xibSetUp() {
        // setup the view from .xib
        view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        // grabs the appropriate bundle
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
