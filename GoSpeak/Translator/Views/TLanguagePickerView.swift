//
//  TLanguagePickerView.swift
//  Translator
//
//  Created by Beniamin Muntean on 18/05/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit
import Speech

public class TLanguagePickerView: UIView, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet public weak var pickerView: UIPickerView!
    public var view: UIView!
    let nibName = "TLanguagePickerView"
    public var delegate: TLanguagePickerDelegate?
    public var languageTag: Int?
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetUp()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetUp()
    }
    
    
    func xibSetUp() {
        // setup the view from .xib
        view = loadViewFromNib()
        view.frame = self.bounds
        view.backgroundColor = UIColor(displayP3Red: 239.0 / 255, green: 239.0 / 255, blue: 239.0 / 255, alpha: 0.0)
        self.backgroundColor = UIColor(displayP3Red: 239.0 / 255, green: 239.0 / 255, blue: 239.0 / 255, alpha: 0.0)
        pickerView.delegate = self
        pickerView.dataSource =  self
        pickerView.backgroundColor = UIColor(displayP3Red: 239.0 / 255, green: 239.0 / 255, blue: 239.0 / 255, alpha: 0.0)
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        // grabs the appropriate bundle
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

    @IBAction func doneButtonPressed(_ sender: Any) {
        let selectedLanguage = TLanguageManager.sharedInstance.languagesDataSource[pickerView.selectedRow(inComponent: 0)]
        self.delegate?.languagePickerDidFinishSelectLanguage(languagePiker: self, language: selectedLanguage, languageTag: self.languageTag!)
    }
    
    // MARK: - PickerViewDelegate methods
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return TLanguageManager.sharedInstance.languagesDataSource.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let customView = TPickerRowView.init(frame: CGRect(x: 0.0, y: 0.0, width: pickerView.frame.width, height: 100.0))
        let languageObj = TLanguageManager.sharedInstance.languagesDataSource[row]
        customView.languageNameLabel.text = languageObj.languageName
        customView.flagImageView.image = UIImage(named:languageObj.languageRectangleFlagIconName)
        
        return customView
    }
}
