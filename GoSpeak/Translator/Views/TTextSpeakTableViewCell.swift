//
//  TTextSpeakTableViewCell.swift
//  Translator
//
//  Created by Beniamin Muntean on 16/05/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit

public class TTextSpeakTableViewCell: UITableViewCell {

    @IBOutlet public weak var flagImageView: UIImageView!
    @IBOutlet public weak var languageNameLabel: UILabel!
    @IBOutlet public weak var textView: UITextView!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func soundButtonPressed(_ sender: Any) {
        TAudioManager.sharedInstance.speakText(text: textView.text,language: (TLanguageManager.sharedInstance.destinationLanguage?.languageIdentifier)!)
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [textView.text], applicationActivities: nil);
        let currentViewController:UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        currentViewController.present(activityViewController, animated: true, completion: nil);
    }
}
