//
//  ViewController.swift
//  Translator
//
//  Created by Beniamin Muntean on 16/05/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit
import goSpeakFramework

class ViewController: UIViewController, TAudioDelegate, UITableViewDelegate, UITableViewDataSource, TLanguagePickerDelegate {

    @IBOutlet weak var languagePickerView: TLanguagePickerView!
    @IBOutlet weak var leftFlagButtonView: UIButton!
    @IBOutlet weak var rightFlagButtonView: UIButton!
    @IBOutlet weak var leftRecordButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var rightRecordButton: UIButton!
    var blurEffectView: UIView!
    
    
    var dataSource = [NSObject]()
    var buttonEnabled: Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.languagePickerView.isHidden = true
        self.languagePickerView.delegate = self
        tableView.transform = CGAffineTransform(rotationAngle: -(CGFloat)(Double.pi))
        tableView.delegate = self
        tableView.dataSource = self
        
        if (TLanguageManager.sharedInstance.leftLanguage != nil) {
            self.leftFlagButtonView.setImage(UIImage(named: (TLanguageManager.sharedInstance.leftLanguage?.languageRectangleFlagIconName)!), for: .normal)
        }
        
        if (TLanguageManager.sharedInstance.rightLanguage != nil) {
            self.rightFlagButtonView.setImage(UIImage(named: (TLanguageManager.sharedInstance.rightLanguage?.languageRectangleFlagIconName)!), for: .normal)
        }
        
        TAudioManager.sharedInstance.delegate = self;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func leftRecordButtonPressed(_ sender: Any) {
        if TCheckInternet.Connection() {
            if buttonEnabled {
                buttonEnabled = false
                
                if let image = UIImage(named: "stop-record") {
                    leftRecordButton.setImage(image, for: .normal)
                }
                TLanguageManager.sharedInstance.sourceLanguage = TLanguageManager.sharedInstance.leftLanguage
                TLanguageManager.sharedInstance.destinationLanguage = TLanguageManager.sharedInstance.rightLanguage
                TAudioManager.sharedInstance.startRecording(language: (TLanguageManager.sharedInstance.leftLanguage?.languageIdentifier)!)
            } else {
                TAudioManager.sharedInstance.stopRecording()
                
                if let image = UIImage(named: "mic-blue") {
                    leftRecordButton.setImage(image, for: .normal)
                }
                buttonEnabled = true
            }
        } else {
            self.Alert(message: "Please check you internet connection and try again.")
        }
    }
    
    @IBAction func rightRecordButtonPressed(_ sender: Any) {
        if TCheckInternet.Connection() {
            if buttonEnabled {
                buttonEnabled = false
                
                if let image = UIImage(named: "stop-record") {
                    rightRecordButton.setImage(image, for: .normal)
                }
                TLanguageManager.sharedInstance.sourceLanguage = TLanguageManager.sharedInstance.rightLanguage
                TLanguageManager.sharedInstance.destinationLanguage = TLanguageManager.sharedInstance.leftLanguage
                TAudioManager.sharedInstance.startRecording(language: (TLanguageManager.sharedInstance.rightLanguage?.languageIdentifier)!)
            } else {
                TAudioManager.sharedInstance.stopRecording()
                
                if let image = UIImage(named: "mic-blue") {
                    rightRecordButton.setImage(image, for: .normal)
                }
                buttonEnabled = true
            }
        } else {
            self.Alert(message: "Please check you internet connection and try again.")
        }
    }
    
    @IBAction func rightButtonViewAction(_ sender: Any) {
        self.languagePickerView.languageTag = 1
        addBackgroundBlurEffect()
        self.view.bringSubviewToFront(self.languagePickerView)
        let languageIndex = TLanguageManager.sharedInstance.getIndexFor(language: TLanguageManager.sharedInstance.rightLanguage!)
        self.languagePickerView.pickerView.selectRow(languageIndex, inComponent: 0, animated: false)
        self.languagePickerView.isHidden = false
    }
    
    @IBAction func leftButtonViewAction(_ sender: Any) {
        self.languagePickerView.languageTag = 0
        addBackgroundBlurEffect()
        self.view.bringSubviewToFront(self.languagePickerView)
        let languageIndex = TLanguageManager.sharedInstance.getIndexFor(language: TLanguageManager.sharedInstance.leftLanguage!)
        self.languagePickerView.pickerView.selectRow(languageIndex, inComponent: 0, animated: false)
        self.languagePickerView.isHidden = false
    }
    // MARK: - TAudioManager delegate methods
    
    func didFinishRecording(text: String) {
        let translator : FGTranslator = FGTranslator.init(googleAPIKey: "AIzaSyAIHYHWTcm7aQGy3LnLUfHXLkUJr36adsw")       
        let sourceLanguage: String = (TLanguageManager.sharedInstance.sourceLanguage?.languageCode)!
        let destinationLanguage: String = (TLanguageManager.sharedInstance.destinationLanguage?.languageCode)!
        translator.translateText(text, withSource: sourceLanguage, target: destinationLanguage) { (error, translatedString, sourceLanguage) in
            if (error == nil) {
                let flagIconName = TLanguageManager.sharedInstance.destinationLanguage?.languageRoundFlagIconName
                let languageName = TLanguageManager.sharedInstance.destinationLanguage?.languageName
                let object: TranslatedObject = TranslatedObject(flagIconName: flagIconName!, languageName: languageName!, message: translatedString!)
                TAudioManager.sharedInstance.speakText(text: object.message, language: (TLanguageManager.sharedInstance.destinationLanguage?.languageIdentifier)!)
                self.dataSource.insert(object, at: 0)
                self.tableView.reloadData()
            }
        }
    }
    
    func didRetrievePartialResult(text: String) {
        //show recorded message
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "textToSpeakCell", for: indexPath) as! TTextSpeakTableViewCell
        let tObject: TranslatedObject = dataSource[indexPath.row] as! TranslatedObject
        
        cell.languageNameLabel.text = tObject.languageName
        cell.flagImageView.image = UIImage(named: tObject.flagIconName)
        cell.textView.text = tObject.message
        cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
        
        return cell
    }
    
    // MARK: - TLanguagePickerDelegate
    
    func languagePickerDidFinishSelectLanguage(languagePiker: TLanguagePickerView, language: TLanguageObject, languageTag: Int) {
        if languageTag == 0 {
            TLanguageManager.sharedInstance.leftLanguage = language
            self.leftFlagButtonView.setImage(UIImage(named: (language.languageRectangleFlagIconName)), for: .normal)
        } else {
            TLanguageManager.sharedInstance.rightLanguage = language
            self.rightFlagButtonView.setImage(UIImage(named: (language.languageRectangleFlagIconName)), for: .normal)
        }
        
        self.languagePickerView.isHidden = true
        removeBackgroundBlurEffect()
    }
    
    func addBackgroundBlurEffect() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        view.addSubview(blurEffectView)
    }
    
    func removeBackgroundBlurEffect() {
        blurEffectView.removeFromSuperview()
    }
    
    func Alert(message: String) {
        let alert = UIAlertController(title: "No Internet Connection", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func sharedButtonPressed(_ sender: Any) {
        
        
    }
}
