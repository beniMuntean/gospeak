//
//  MessagesViewController.swift
//  GoSpeakMessage
//
//  Created by Beniamin Muntean on 07/08/2018.
//  Copyright © 2018 Beniamin Muntean. All rights reserved.
//

import UIKit
import goSpeakFramework
import Messages

class MessagesViewController: MSMessagesAppViewController, TLanguagePickerDelegate, TAudioDelegate {
    
    @IBOutlet weak var languagePickerView: TLanguagePickerView!
    @IBOutlet weak var leftFlagButtonView: UIButton!
    @IBOutlet weak var rightFlagButtonView: UIButton!
    @IBOutlet weak var leftRecordButton: UIButton!
    @IBOutlet weak var recordedTextView: UITextView!
    var blurEffectView: UIView!
    var dataSource = [NSObject]()
    var buttonEnabled: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TAudioManager.sharedInstance.checkSpeechAuthorization()
        
        var leftLanguageIdentifier =  UserDefaults.standard.string(forKey: "leftLanguageIdentifier")
        var rightLanguageIdentifier =  UserDefaults.standard.string(forKey: "rightLanguageIdentifier")
        
        if (leftLanguageIdentifier == nil) {
            leftLanguageIdentifier = "en-US"
        }
        
        if (rightLanguageIdentifier == nil) {
            rightLanguageIdentifier = "es-ES"
        }
        
        TLanguageManager.sharedInstance.leftLanguage = TLanguageManager.sharedInstance.getLanguageObjectFor(languageIdentifier: leftLanguageIdentifier)
        TLanguageManager.sharedInstance.rightLanguage = TLanguageManager.sharedInstance.getLanguageObjectFor(languageIdentifier: rightLanguageIdentifier)
        
        self.languagePickerView.isHidden = true
        self.languagePickerView.delegate = self
        
        if (TLanguageManager.sharedInstance.leftLanguage != nil) {
            self.leftFlagButtonView.setImage(UIImage(named: (TLanguageManager.sharedInstance.leftLanguage?.languageRectangleFlagIconName)!), for: .normal)
        }
        
        if (TLanguageManager.sharedInstance.rightLanguage != nil) {
            self.rightFlagButtonView.setImage(UIImage(named: (TLanguageManager.sharedInstance.rightLanguage?.languageRectangleFlagIconName)!), for: .normal)
        }
        
        TAudioManager.sharedInstance.delegate = self;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftRecordButtonPressed(_ sender: Any) {
        if TCheckInternet.Connection() {
            if buttonEnabled {
                buttonEnabled = false
                
                if let image = UIImage(named: "stop-record") {
                    leftRecordButton.setImage(image, for: .normal)
                }
                TLanguageManager.sharedInstance.sourceLanguage = TLanguageManager.sharedInstance.leftLanguage
                TLanguageManager.sharedInstance.destinationLanguage = TLanguageManager.sharedInstance.rightLanguage
                TAudioManager.sharedInstance.startRecording(language: (TLanguageManager.sharedInstance.leftLanguage?.languageIdentifier)!)
            } else {
                TAudioManager.sharedInstance.stopRecording()
                
                if let image = UIImage(named: "mic-blue") {
                    leftRecordButton.setImage(image, for: .normal)
                }
                buttonEnabled = true
            }
        } else {
            self.Alert(message: "Please check your internet connection and try again.")
        }
    }
    
    @IBAction func rightButtonViewAction(_ sender: Any) {
        self.languagePickerView.languageTag = 1
        addBackgroundBlurEffect()
        self.view.bringSubviewToFront(self.languagePickerView)
        let languageIndex = TLanguageManager.sharedInstance.getIndexFor(language: TLanguageManager.sharedInstance.rightLanguage!)
        self.languagePickerView.pickerView.selectRow(languageIndex, inComponent: 0, animated: false)
        self.languagePickerView.isHidden = false
    }
    
    @IBAction func leftButtonViewAction(_ sender: Any) {
        self.languagePickerView.languageTag = 0
        addBackgroundBlurEffect()
        self.view.bringSubviewToFront(self.languagePickerView)
        let languageIndex = TLanguageManager.sharedInstance.getIndexFor(language: TLanguageManager.sharedInstance.leftLanguage!)
        self.languagePickerView.pickerView.selectRow(languageIndex, inComponent: 0, animated: false)
        self.languagePickerView.isHidden = false
    }
    
    // MARK: - Conversation Handling
    
    override func willBecomeActive(with conversation: MSConversation) {
        // Called when the extension is about to move from the inactive to active state.
        // This will happen when the extension is about to present UI.
        
        // Use this method to configure the extension and restore previously stored state.
    }
    
    override func didResignActive(with conversation: MSConversation) {
        // Called when the extension is about to move from the active to inactive state.
        // This will happen when the user dissmises the extension, changes to a different
        // conversation or quits Messages.
        
        // Use this method to release shared resources, save user data, invalidate timers,
        // and store enough state information to restore your extension to its current state
        // in case it is terminated later.
    }
   
    override func didReceive(_ message: MSMessage, conversation: MSConversation) {
        // Called when a message arrives that was generated by another instance of this
        // extension on a remote device.
        
        // Use this method to trigger UI updates in response to the message.
    }
    
    override func didStartSending(_ message: MSMessage, conversation: MSConversation) {
        // Called when the user taps the send button.
    }
    
    override func didCancelSending(_ message: MSMessage, conversation: MSConversation) {
        // Called when the user deletes the message without sending it.
    
        // Use this to clean up state related to the deleted message.
    }
    
    override func willTransition(to presentationStyle: MSMessagesAppPresentationStyle) {
        // Called before the extension transitions to a new presentation style.
    
        // Use this method to prepare for the change in presentation style.
    }
    
    override func didTransition(to presentationStyle: MSMessagesAppPresentationStyle) {
        // Called after the extension transitions to a new presentation style.
    
        // Use this method to finalize any behaviors associated with the change in presentation style.
    }
    
    
    
    // MARK: - TLanguagePickerDelegate
    
    func languagePickerDidFinishSelectLanguage(languagePiker: TLanguagePickerView, language: TLanguageObject, languageTag: Int) {
        if languageTag == 0 {
            TLanguageManager.sharedInstance.leftLanguage = language
            self.leftFlagButtonView.setImage(UIImage(named: (language.languageRectangleFlagIconName)), for: .normal)
            UserDefaults.standard.set(TLanguageManager.sharedInstance.leftLanguage?.languageIdentifier, forKey: "leftLanguageIdentifier")
        } else {
            TLanguageManager.sharedInstance.rightLanguage = language
            self.rightFlagButtonView.setImage(UIImage(named: (language.languageRectangleFlagIconName)), for: .normal)
            UserDefaults.standard.set(TLanguageManager.sharedInstance.rightLanguage?.languageIdentifier, forKey: "rightLanguageIdentifier")
        }
        
        self.languagePickerView.isHidden = true
        removeBackgroundBlurEffect()
    }
    
    // MARK: - TAudioManager delegate methods
    
    func didFinishRecording(text: String) {
        let translator : FGTranslator = FGTranslator.init(googleAPIKey: "AIzaSyAIHYHWTcm7aQGy3LnLUfHXLkUJr36adsw")
        let sourceLanguage: String = (TLanguageManager.sharedInstance.sourceLanguage?.languageCode)!
        let destinationLanguage: String = (TLanguageManager.sharedInstance.destinationLanguage?.languageCode)!
        translator.translateText(text, withSource: sourceLanguage, target: destinationLanguage) { (error, translatedString, sourceLanguage) in
            if (error == nil) {
                let flagIconName = TLanguageManager.sharedInstance.destinationLanguage?.languageRoundFlagIconName
                let languageName = TLanguageManager.sharedInstance.destinationLanguage?.languageName
                let object: TranslatedObject = TranslatedObject(flagIconName: flagIconName!, languageName: languageName!, message: translatedString!)
                self.composeMessage(translatedObject: object)
            }
        }
    }
    
    func didRetrievePartialResult(text: String) {
        self.recordedTextView.text = text
    }
    
    // MARK: - Private methods
    
    func Alert(message: String) {
        let alert = UIAlertController(title: "No Internet Connection", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func composeMessage(translatedObject: TranslatedObject) {
//        let layout = MSMessageTemplateLayout()
//        layout.image = UIImage(named: translatedObject.flagIconName)
//        layout.caption = translatedObject.message
//
//        let message = MSMessage()
//        message.shouldExpire = false
//        message.layout = layout
        
        self.activeConversation?.insertText(translatedObject.message, completionHandler: nil)
    }
    
    func addBackgroundBlurEffect() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        view.addSubview(blurEffectView)
    }
    
    func removeBackgroundBlurEffect() {
        blurEffectView.removeFromSuperview()
    }

}
